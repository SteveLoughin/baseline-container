### Baseline Container

This project is a baseline for building a dockerized container stack that serves a vuejs 
web site that relies upon backend data supplied (mostly) by a django rest framework api. 

#### Authentication
The web site is assumed to operate within the American Reading Company's eco-system in 
order to authenticate users using LDAP.

#### Front End
The front end as provided here is sort of a minimal VueJS site that demonstrates how to 
authenticate users and then allow them to access data through Vuex to an API.

#### Django RestFramework API
The API is implemented using the django restframework.  The minimalistic setup uses 
Sqlite3 as the database, though it is possible (and not difficult) to obtain the data 
from an external SQL server that is "outside" of the docker-container stack.  The 
api also can be accessed directly through django's "browsable" api by making calls to

http://localhost:8000/api/books/ 

for example.

There is one admin (superuser) for the browsable api (not LDAP)
* username: sloughin   
* password: savant1d

#### Installation
git clone git@bitbucket.org:SteveLoughin/baseline-container.git

docker-comnpose build
docker-compose up

Open a browser and connect to http://0.0.0.0:8000/web/index.html

To work on vue development you will want to use a python virtual environment as well as
npm and webpack etc.  The files supporting these were listed in .gitignore so best to
reinstall them if you want to do development using this as the baseline.

#### Python Virtual Environment: 
1. Python 3.8.3  
  * download Python 3.8.3 from https://www.python.org/downloads/release/python-383/
  * install pipenv : https://www.codingforentrepreneurs.com/blog/install-django-on-mac-or-linux
  * install django, djangorestframework & other packages listed in Pipfile
  
#### VueJS / npm:
1. NPM
  * General instructions : https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
  * install both nodejs and npm
  * cd to vuesite (or whatever you rename it to)
  * npm init -- runs an initialization script
  
2. VueJS
  * navigate to the vuesite folder
  * npm install --save vue
  * npm install --save vuex
  * npm install -D webpack webpack-cli vue-loader vue-template-compiler vue-style-loader css-loader
  * npm install -D @babel/core babel-loader @babel/preset-env @babel/plugin-syntax-dynamic-import
  