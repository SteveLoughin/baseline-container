from django.urls import path

from .views import BookList, BookDetail

urlpatterns = [
    path('book/<int:pk>/', BookDetail.as_view()),
    path('books/', BookList.as_view()),
]
