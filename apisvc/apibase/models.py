from django.db import models

# Create your models here.
class Book(models.Model):
    # id - pk
    title = models.CharField(max_length=80)
    author = models.CharField(max_length=80)
    isbn = models.CharField(max_length=15)

    def __str__(self):
        return f"{self.title} by {self.author}"
